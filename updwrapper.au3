;--------------------------------------------------------------
;--------------- НАЧАЛО ОСНОВНОГО ТЕЛА СКРИПТА ----------------
#include <Date.au3>
#include <File.au3>

$debugmode = IniRead("params.ini", "params", "debugmode", "0")
If $debugmode = 1 Then
	$debuglog = @YEAR & "_" & @MON & "_" & @MDAY & "_" & @HOUR & "_" & @MIN & "_" & @SEC & "_" & @MSEC & ".log"
	AddToLog($debuglog, "ЗАПУСК СКРИПТА. Дебаг-режим включен.")
EndIf

local $updpath[5]
$fullpath = IniRead("params.ini", "params", "fullpath", "C:\client2016\updater.exe")
$updpath = _PathSplit($fullpath, "", "", "", "")

$processname = IniRead("params.ini", "params", "processname", $updpath[3] & $updpath[4])
$windowtitle = IniRead("params.ini", "params", "windowtitle", "Обновление клиентской части")

; Проверяем наличие в памяти процесса updater.exe
If $debugmode = 1 Then AddToLog($debuglog, "Проверяем наличие в памяти процесса <" & $processname & ">...")
If ProcessExists($processname) Then
	If $debugmode = 1 Then AddToLog($debuglog, "Процесс " & $processname & " существует.")
	;Если процесс в памяти - проверяем, есть ли окно процесса в списке окон (по заголовку)
	If $debugmode = 1 Then AddToLog($debuglog, "Проверяем наличие окна <" & $windowtitle & ">...")
	If WinExists($windowtitle) Then
		If $debugmode = 1 Then AddToLog($debuglog, "Окно " & $windowtitle & " существует.")
		;Если есть и то и то - проверяем время последнего запуска
		If $debugmode = 1 Then AddToLog($debuglog, "Проверяем время последнего запуска...")
		$laststarttime = IniRead("params.ini", "time", "laststart", "0")
		If $laststarttime > 0 Then
			If $debugmode = 1 Then AddToLog($debuglog, "Время последнего запуска больше 0.")
			$timepassed = TimerDiff($laststarttime)
			$maxperiod = IniRead("params.ini", "params", "maxperiod", "90")
			
			If $debugmode = 1 Then AddToLog($debuglog, "Оцениваем, сколько прошло со времени последнего запуска...")
			If ($timepassed / 60000) > $maxperiod Then
				;Время работы превысило критический предел - гасим процесс, goto start
				If $debugmode = 1 Then AddToLog($debuglog, "Время с последнего запуска превысило критический предел - гасим процесс...")
				RecursiveCloseProcess($processname)
			Else
				;Если время не превышено - считаем, что процесс отрабатывает загрузку, Exit
				If $debugmode = 1 Then AddToLog($debuglog, "Время с последнего запуска НЕ превысило критический предел - ВЫХОД ИЗ СКРИПТА.")
				Exit
			EndIf
		Else
			;Время старта еще не фиксировалось, но процесс в памяти (и активное окно) уже есть. Гасим, then goto start.
			If $debugmode = 1 Then AddToLog($debuglog, "Время старта еще не фиксировалось в ini-файле, а процесс уже существует. Гасим процесс...")
			RecursiveCloseProcess($processname)
		EndIf
	Else
		;Если процесс есть, а окна нет - гасим процесс, goto start
		If $debugmode = 1 Then AddToLog($debuglog, "Процесс в памяти есть, а окна в списке окон нет. Гасим процесс...")
		RecursiveCloseProcess($processname)
	EndIf
Else
	If $debugmode = 1 Then AddToLog($debuglog, "Процесс " & $processname & " НЕ существует.")
EndIf

If $debugmode = 1 Then AddToLog($debuglog, "Запускаем updater.exe (путь запуска " & $fullpath & ", рабочий каталог " & $updpath[1] & $updpath[2] & "...")
Run($fullpath, $updpath[1] & $updpath[2])
If $debugmode = 1 Then AddToLog($debuglog, "Команда на запуск updater.exe отдана. Выжидаем 5 секунд...")
Sleep(5000)

If ProcessExists($processname) = 0 Then
	If $debugmode = 1 Then AddToLog($debuglog, "Процесс " & $processname & " не обнаружен в памяти! Проверьте настройки params.ini! ЗАВЕРШЕНИЕ РАБОТЫ СКРИПТА.")
	Exit
EndIf

$noconnmessage = False
;При запуске сразу проверяется соединение с сервером. Если сервер недоступен - выводится MsgBox с соотв. сообщением
If $debugmode = 1 Then AddToLog($debuglog, "Проверяем наличие окна <Нет соединения с сервером>...")
If WinExists($windowtitle, "Нет соединения с сервером приложений") Then
	If $debugmode = 1 Then AddToLog($debuglog, "Окно <Нет соединения с сервером> существует. Жмем в нем ОК...")
	;Посылаем нажатие кнопке "Ок", чтобы закрыть MsgBox
	ControlClick($windowtitle, "Нет соединения с сервером приложений","[CLASS:Button; INSTANCE:1]")
	$noconnmessage = True
Else
	If $debugmode = 1 Then AddToLog($debuglog, "Окна <Нет соединения с сервером> НЕ существует.")
EndIf

If $debugmode = 1 Then AddToLog($debuglog, "Проверяем наличие окна <Не удалось подключиться к серверу приложения>...")
If WinExists($windowtitle, "Не удалось подключиться к серверу приложения") Then
	If $debugmode = 1 Then AddToLog($debuglog, "Окно <Не удалось подключиться к серверу приложения> существует. Жмем в нем ОК...")
	;Посылаем нажатие кнопке "Ок", чтобы закрыть MsgBox
	ControlClick($windowtitle, "Не удалось подключиться к серверу приложения","[CLASS:Button; INSTANCE:1]")
	$noconnmessage = True
Else
	If $debugmode = 1 Then AddToLog($debuglog, "Окна <Не удалось подключиться к серверу приложения> НЕ существует.")
EndIf

;Если было сообщение "Нет соединения с сервером" - выжидаем на всякий случай 5 секунд
If $noconnmessage = True Then
	If $debugmode = 1 Then AddToLog($debuglog, "Если было сообщение <Нет соединения> - выжидаем 5 секунд...")
	Sleep(5000)
EndIf

;Сохраняем в ini время последнего старта
If $debugmode = 1 Then AddToLog($debuglog, "Сохраняем в ini время последнего старта...")
IniWrite("params.ini", "time", "laststart", TimerInit())

$goout = False
$falsescounter = 0
$clickOK = True
$success = False
If $debugmode = 1 Then AddToLog($debuglog, "Запускаем бесконечный цикл проверки работы updater.exe...")
Do 
	;Жмем ОК для запуска попытки скачивания обновления
	If $clickOK = True Then 
		If $debugmode = 1 Then AddToLog($debuglog, "Жмем ОК в главном окне updater.exe для запуска попытки скачивания обновления...")
		ControlClick($windowtitle, "Тип обновления", "[CLASS:TButton; INSTANCE:2]")
		If $debugmode = 1 Then AddToLog($debuglog, "Нажали.")
		$clickOK = False
	EndIf
	
	If $debugmode = 1 Then AddToLog($debuglog, "Выжидаем заданное в ini-файле время...")
	Sleep(1000 * IniRead("params.ini", "time", "pausebetweenchecks", "10"));Выжидаем время
	If $debugmode = 1 Then AddToLog($debuglog, "Проверяем наличие окна <Процесс обновления завершен>...")
	If WinExists($windowtitle, "Процесс обновления завершен") Then ;Информ. окно "Обновление завершено". 
		If $debugmode = 1 Then AddToLog($debuglog, "Окно <Процесс обновления завершен> существует. Сохраняем время последнего завершения, закрываем окно нажатием ОК...")
		;Сохраняем в ini время последнего успешного завершения, кликаем ОК - после этого updater.exe сам закрывается
		IniWrite("params.ini", "time", "lastfinish", TimerInit())
		ControlClick($windowtitle, "Процесс обновления завершен","[CLASS:Button; INSTANCE:1]")
		$clickOK = False
		$goout = True
		$success = True
	Else
		If $debugmode = 1 Then AddToLog($debuglog, "Окно <Процесс обновления завершен> НЕ существует.")
		;Основная ошибка - MessageBox "Не удалось подключиться к серверу приложения" с единственной кнопкой ОК
		If $debugmode = 1 Then AddToLog($debuglog, "Проверяем наличие окна <Не удалось подключиться к серверу приложения>...")
		If WinExists($windowtitle, "Не удалось подключиться к серверу приложения") Then
			If $debugmode = 1 Then AddToLog($debuglog, "Окно <Не удалось подключиться к серверу приложения> существует...")
			;Жмем ОК в мессаджбоксе - он закрывается, окно updater.exe остается на экране. 
			;Снова нажимаем "ОК" в окне updater.exe для очередной попытки скачать файлы обновления:
			If $debugmode = 1 Then AddToLog($debuglog, "Жмем ОК в окне <Не удалось подключиться к серверу приложения>...")
			ControlClick($windowtitle, "Не удалось подключиться к серверу приложения","[CLASS:Button; INSTANCE:1]")
			Sleep(200)
			$falsescounter += 1
			$clickOK = True
		Else
			If $debugmode = 1 Then AddToLog($debuglog, "Окна <Не удалось подключиться к серверу приложения> НЕ существует.")
		EndIf
		
		;Еще одна ошибка (когда соединение рвется уже после установления коннекта). Единственная кнопка ОК.
		;Текст ошибки: Не удалось установить соединение с сервером приложений
		If $debugmode = 1 Then AddToLog($debuglog, "Проверяем наличие окна <Ошибка в процессе обновления>...")
		If WinExists($windowtitle, "Ошибка в процессе обновления") Then
			If $debugmode = 1 Then AddToLog($debuglog, "Окно <Ошибка в процессе обновления> существует. Жмем ОК в нем...")
			;Жмем ОК в мессаджбоксе - он закрывается, окно updater.exe остается на экране. 
			;Снова нажимаем "ОК" в окне updater.exe для очередной попытки скачать файлы обновления:
			ControlClick($windowtitle, "Ошибка в процессе обновления","[CLASS:Button; INSTANCE:1]")
			Sleep(200)
			$falsescounter += 1
			$clickOK = True
		Else
			If $debugmode = 1 Then AddToLog($debuglog, "Окна <Ошибка в процессе обновления> НЕ существует.")
		EndIf
		
		;Когда связь рвется уже после начала скачивания файлов (в процессе скачивания) - выдается мессаджбокс "Да-Нет":
		;Нет соединения с сервером приложений. Попробовать восстановить?
		;Жмем "Да"
		If $debugmode = 1 Then AddToLog($debuglog, "Проверяем наличие окна <Попробовать восстановить?> - возникает, если проблемы со связью возникли уже после начала скачивания файлов. Имеет кнопки Да и Нет.")
		If WinExists($windowtitle, "Попробовать восстановить?") Then
			;Жмем Да в мессаджбоксе - он закрывается, апдейтер пытается соединиться с сервером.
			If $debugmode = 1 Then AddToLog($debuglog, "Окно <Попробовать восстановить?> существует. Жмем Да...")
			ControlClick($windowtitle, "Попробовать восстановить?","[CLASS:Button; INSTANCE:1]")
			Sleep(200)
			$falsescounter += 1
			$clickOK = False ;Жать "ОК" в родительском updater.exe не нужно - попытка запускается уже после нажатия "Да"
		Else
			If $debugmode = 1 Then AddToLog($debuglog, "Окна <Попробовать восстановить?> НЕ существует.")
		EndIf
		
		If $debugmode = 1 Then AddToLog($debuglog, "Число неудачных попыток обновления: " & $falsescounter)
		If $falsescounter > IniRead("params.ini", "params", "maxtries", "10") Then
			If $debugmode = 1 Then AddToLog($debuglog, "Число неудачных попыток обновления превысило заданное настройками. Выходим...")
			$clickOK = False
			$goout = True
		EndIf
	EndIf
Until $goout = True

Sleep(500)
;Закрываем updater.exe. После успешного запуска он и сам закроется (после нажатия ОК в MessageBox'е, информирующем
;    об успешном завершении обновления) - а если в процессе скачивания  обновы были ошибки, лучше убедиться, что в 
;    памяти не осталось лишней копии updater.exe.
If $success = True Then
	If WinExists($windowtitle, "Процесс обновления завершен") Then ;Информ. окно "Обновление завершено" иногда может не закрываться с первого раза (1-й раз - в цикле While). Закрываем еще раз...
		If $debugmode = 1 Then AddToLog($debuglog, "Окно <Процесс обновления завершен> всё ещё существует. Снова жмем ОК...")
		ControlClick($windowtitle, "Процесс обновления завершен","[CLASS:Button; INSTANCE:1]")
		Sleep(200)
	EndIf
EndIf
		
If $falsescounter > 0 Then 
	If $debugmode = 1 Then AddToLog($debuglog, "Если неудачных попыток больше нуля - на всякий случай запускаем прибивку лишних процессов в памяти...")
	RecursiveCloseProcess($processname)
EndIf

;Если нужно, синхронизируем каталоги
If IniRead("params.ini", "params", "syncdirs", "0") = 1 Then
	If $debugmode = 1 Then AddToLog($debuglog, "Настройками задано синхронизировать каталоги. Проверяем, успешно ли прошло обновление...")
	If $success = True Then ;Только в случае успешно прошедшего обновления
		If $debugmode = 1 Then AddToLog($debuglog, "Обновление прошло успешно. Запускаем синхрон...")
		DirCopy(IniRead("params.ini", "params", "dirfrom", $updpath[1] & $updpath[2]), IniRead("params.ini", "params", "dirto", "C:\client_2016_shared"), 1)
		If $debugmode = 1 Then AddToLog($debuglog, "Синхрон прошел.")
	EndIf
EndIf

If $debugmode = 1 Then AddToLog($debuglog, "ЗАВЕРШЕНИЕ СКРИПТА.")
Exit

;--------------- КОНЕЦ ОСНОВНОГО ТЕЛА СКРИПТА ----------------
;-------------------------------------------------------------



;-------------------------------------------------------------
;------------------ ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ ------------------

;--- Вспомогательная функция рекурсивного прибития процесса updater.exe, висящего в памяти
Func RecursiveCloseProcess($processname, $counter=0)
	If $counter > 9 Then Return
	If ProcessExists($processname) Then 
		ProcessClose($processname)
		Sleep(3000)
	EndIf
	If ProcessExists($processname) Then 
		RecursiveCloseProcess($processname, $counter+1)
	EndIf
EndFunc

;Запись логов. Используется при включенном дебаг-режиме
Func AddToLog($filename, $text)
	$file = FileOpen($filename, 1)
	If $file <> -1 Then
		FileWriteLine($file, _Now() & ": " & $text)
		FileClose($file)
	EndIf
EndFunc